FROM tomcat:8.0-alpine

LABEL maintainer="ejemplo@ejemplo.net"

ADD ./target/TodoList_CI.war /usr/local/tomcat/webapps/

EXPOSE 8080

CMD ["catalina.sh", "run"]