CREATE SCHEMA IF NOT EXISTS `db_todolist` DEFAULT CHARACTER SET utf8 ;
USE `db_todolist`;
CREATE TABLE IF NOT EXISTS `db_todolist`.`tareas` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`tarea` VARCHAR(45) NULL,
	`detalle` VARCHAR(400) NULL,
	`fecha_creacion` DATETIME NULL,
	`fecha_vencimiento` DATETIME NULL,
	`estado` VARCHAR(45) NULL,
	PRIMARY KEY (`id`)
)ENGINE = InnoDB;