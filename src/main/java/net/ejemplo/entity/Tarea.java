package net.ejemplo.entity;

import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

public class Tarea {

    private int id;
    
    /*@NotEmpty
    @Size(min = 1, max = 20)*/
    private String tarea;
    
    /*@NotEmpty(message = "Password must not be blank.")
    @Size(min = 1, max = 10, message = "Password must between 1 to 10 Characters.")*/
    private String detalle;
    private String fecha_creacion;
    private String fecha_vencimiento;
    private String estado;

    public Tarea() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTarea() {
        return tarea;
    }

    public void setTarea(String tarea) {
        this.tarea = tarea;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Tarea{" + "id=" + id + ", tarea=" + tarea + ", detalle=" + detalle + ", fecha_creacion=" + fecha_creacion + ", fecha_vencimiento=" + fecha_vencimiento + ", estado=" + estado + '}';
    }

    

}
